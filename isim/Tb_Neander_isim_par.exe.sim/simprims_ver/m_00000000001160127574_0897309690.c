/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif




extern void simprims_ver_m_00000000001160127574_0897309690_4154103205_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_4154103205", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_4154103205.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_2457174477_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_2457174477", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_2457174477.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_0909904551_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_0909904551", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_0909904551.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_0705448992_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_0705448992", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_0705448992.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_3953840930_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_3953840930", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_3953840930.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_1340299336_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_1340299336", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_1340299336.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_0801107203_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_0801107203", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_0801107203.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_1449181164_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_1449181164", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_1449181164.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_0124893903_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_0124893903", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_0124893903.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_3132872193_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_3132872193", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_3132872193.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_3672597322_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_3672597322", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_3672597322.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_1731006340_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_1731006340", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_1731006340.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_4259889577_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_4259889577", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_4259889577.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_0349770423_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_0349770423", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_0349770423.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_2407292632_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_2407292632", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_2407292632.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_1716358598_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_1716358598", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_1716358598.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_4062786694_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_4062786694", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_4062786694.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_0722713012_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_0722713012", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_0722713012.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_4135936049_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_4135936049", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_4135936049.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_1263471871_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_1263471871", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_1263471871.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_2348181097_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_2348181097", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_2348181097.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_2530858362_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_2530858362", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_2530858362.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_1816123850_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_1816123850", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_1816123850.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_2322994009_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_2322994009", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_2322994009.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_1318187029_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_1318187029", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_1318187029.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_2466720144_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_2466720144", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_2466720144.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_4082867419_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_4082867419", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_4082867419.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_0785204574_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_0785204574", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_0785204574.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_3559606766_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_3559606766", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_3559606766.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_3542018351_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_3542018351", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_3542018351.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_1133477970_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_1133477970", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_1133477970.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_1220257088_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_1220257088", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_1220257088.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_2652461527_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_2652461527", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_2652461527.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_0163418219_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_0163418219", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_0163418219.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_0299065142_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_0299065142", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_0299065142.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_0077848620_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_0077848620", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_0077848620.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_0601044249_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_0601044249", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_0601044249.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_0586700387_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_0586700387", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_0586700387.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_2670869165_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_2670869165", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_2670869165.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_4285435878_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_4285435878", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_4285435878.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_1118102312_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_1118102312", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_1118102312.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_2980642895_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_2980642895", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_2980642895.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_3644144041_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_3644144041", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_3644144041.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_4265995420_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_4265995420", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_4265995420.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_2201414340_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_2201414340", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_2201414340.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_3510562100_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_3510562100", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_3510562100.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_0870512205_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_0870512205", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_0870512205.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_3918917269_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_3918917269", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_3918917269.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_2940968894_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_2940968894", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_2940968894.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_1587557185_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_1587557185", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_1587557185.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_3815399311_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_3815399311", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_3815399311.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_4104665140_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_4104665140", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_4104665140.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_0095318026_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_0095318026", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_0095318026.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_1056736778_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_1056736778", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_1056736778.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_4277074646_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_4277074646", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_4277074646.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_0190550110_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_0190550110", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_0190550110.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_2633980386_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_2633980386", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_2633980386.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_2116240027_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_2116240027", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_2116240027.didat");
}

extern void simprims_ver_m_00000000001160127574_0897309690_1148986557_init()
{
	xsi_register_didat("simprims_ver_m_00000000001160127574_0897309690_1148986557", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000001160127574_0897309690_1148986557.didat");
}

/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif




extern void simprims_ver_m_00000000000648012491_3151998091_2145020544_init()
{
	xsi_register_didat("simprims_ver_m_00000000000648012491_3151998091_2145020544", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000000648012491_3151998091_2145020544.didat");
}

extern void simprims_ver_m_00000000000648012491_3151998091_0354261571_init()
{
	xsi_register_didat("simprims_ver_m_00000000000648012491_3151998091_0354261571", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000000648012491_3151998091_0354261571.didat");
}

extern void simprims_ver_m_00000000000648012491_3151998091_3028728101_init()
{
	xsi_register_didat("simprims_ver_m_00000000000648012491_3151998091_3028728101", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000000648012491_3151998091_3028728101.didat");
}

extern void simprims_ver_m_00000000000648012491_3151998091_1865857453_init()
{
	xsi_register_didat("simprims_ver_m_00000000000648012491_3151998091_1865857453", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000000648012491_3151998091_1865857453.didat");
}

extern void simprims_ver_m_00000000000648012491_3151998091_3893966075_init()
{
	xsi_register_didat("simprims_ver_m_00000000000648012491_3151998091_3893966075", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000000648012491_3151998091_3893966075.didat");
}

extern void simprims_ver_m_00000000000648012491_3151998091_3018650085_init()
{
	xsi_register_didat("simprims_ver_m_00000000000648012491_3151998091_3018650085", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000000648012491_3151998091_3018650085.didat");
}

extern void simprims_ver_m_00000000000648012491_3151998091_4171868292_init()
{
	xsi_register_didat("simprims_ver_m_00000000000648012491_3151998091_4171868292", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000000648012491_3151998091_4171868292.didat");
}

extern void simprims_ver_m_00000000000648012491_3151998091_2391595093_init()
{
	xsi_register_didat("simprims_ver_m_00000000000648012491_3151998091_2391595093", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000000648012491_3151998091_2391595093.didat");
}

extern void simprims_ver_m_00000000000648012491_3151998091_1733935451_init()
{
	xsi_register_didat("simprims_ver_m_00000000000648012491_3151998091_1733935451", "isim/Tb_Neander_isim_par.exe.sim/simprims_ver/m_00000000000648012491_3151998091_1733935451.didat");
}
